# -*- coding: utf-8 -*-
"""Taller Clase 2.ipynb

Original file is located at
    https://colab.research.google.com/drive/1dWmbE09BeRSYnECNecOwvBYoHaklB0HZ

**Session 2: Genetic Algorithm**

This practical session provides the complete implementation of a genetic algorithm.

Feedback may be sent to Juan-Pablo Silva (jpsilva@dcc.uchile.cl), Alexandre Bergel (abergel@dcc.uchile.cl)

The class `GA` given above provides all the necessary to use genetic algorithm to solve a particular problem.

## Changelog

# 03-07-2020
 - Authors: Carlos Gonzalez C. carlgonz@uchile.cl
 - Changes:
    - Use numpy.
    - get_best_individual also return best index
    - eval_population method
    - Add some instance variables
    - Style fixes
# 07-07-2020
 - Authors: Carlos Gonzalez C. carlgonz@uchile.cl
 - Changes:
    - Use numpy in tournament method
# 12-12-2020
 - Authors: Carlos Gonzalez C. carlgonz@uchile.cl
 - Changes:
    - Do not re-evaluate population in tournament method, so the
    tournament is always over self.population
"""

import time
import random
import numpy as np


# Define the main class of the genetic algorithm
class GA(object):
    # pop_size is the size of the population. E.g., 1000
    # mutation_rate is the rate of a gene mutation. E.g., 0.1
    # fitness is a function that takes an individual as argument.
    #   E.g. lambda genes: sum(collections.Counter(genes))
    # individual_factory is a lambda that creates an individual.
    #   E.g., lambda : [ random.randint(0,10) for i in range(4) ]
    # gene_factory is a function that produces a new gene
    # termination_condition is the termination condition that takes as argument the fitness of the best element.
    # If it returns True, then the algorithm ends
    #   E.g., lambda fitness : fitness >= 3
    # max_iter is the maximum number of iterations
    def __init__(self, pop_size, mutation_rate, fitness, individual_factory, gene_factory, termination_condition,
                 max_iter=100, silent=False):
        self.population_size = pop_size
        self.mutation_rate = mutation_rate
        self.fitness_function = fitness
        self.individual_factory = individual_factory
        self.max_iterations = max_iter
        self.termination_condition = termination_condition
        self.gene_factory = gene_factory
        self.population = list()
        self.individual_fitnesses = list()
        self.silent = silent
        self.best_index = None
        self.best_individual = None
        self.current_iteration = 0

        # Internal variables
        self._best_fitness_list = list()
        self._avg_list = list()

    # main method to actually run the algorithm
    # return a tupple best_fitness_list, avg_list, best_individual
    def run(self):
        self.current_iteration = 0

        # We generate the initial population
        ti = time.time()
        self.population = self.generate_population()
        # Evaluate current population
        tbi = time.time()
        self.eval_population()
        tbf = time.time()
        tai = taf = 0

        # We loop if we have not reached the maximum number of iterations and if the termination condition is not met
        while self.current_iteration <= self.max_iterations and not self.termination_condition(self.fitness_function(self.best_individual)):
            self.log("Iter {} of {}. Took {} s".format(self.current_iteration, self.max_iterations, time.time()-ti))
            ti = time.time()
            # Create a new population
            new_population = []
            for _ in range(self.population_size):
                parent1 = self.tournament()
                parent2 = self.tournament()
                new_population.append(self.create_new_individual(parent1, parent2))
            self.population = new_population

            # Evaluate new population
            self.eval_population()
            self.current_iteration += 1

        self.log("Best found is: {}\nFitness: {}. Avg: {}".format(self.best_individual, self._best_fitness_list[-1], self._avg_list[-1]))
        return self.best_individual

    # Logging 
    def log(self, aString):
        if not self.silent:
            print(aString)

    # Evaluate current population (self.population)
    # Update individuals fitness list, best individual and best index
    # Also update performance metrics (for further analysis)
    def eval_population(self):
        self.individual_fitnesses = self.get_fitness(self.population)
        self.best_index, self.best_individual = self.get_best_individual()
        self._best_fitness_list.append(self.individual_fitnesses[self.best_index])
        self._avg_list.append(np.mean(self.individual_fitnesses))
        self.log("Best is: {}. Fitness: {:01.4f}. Avg: {:01.4f}".format(self.best_individual, self._best_fitness_list[-1], self._avg_list[-1]))

    # return the best individual of the current population
    def get_best_individual(self):
        index = np.argmax(self.individual_fitnesses)
        return index, self.population[index]

    # generate the population, made of individuals
    def generate_population(self):
        _population = list()
        for _ in range(self.population_size):
            _population.append(self.individual_factory())
        return _population

    # Return the list of the fitness values for the population
    def get_fitness(self, a_population):
        res = np.array(list(map(self.fitness_function, a_population)))
        return res

    # Obtain the best individual from a tournament on the population
    def tournament(self, k=5):
        competitors = random.sample(range(len(self.population)), k)
        fitness = self.individual_fitnesses[competitors]
        best_index = np.argmax(fitness)
        return self.population[competitors[best_index]]

    # create a new individual from two parents elements
    def create_new_individual(self, parent1, parent2):
        final_child = self.crossover(parent1, parent2)
        # mutation
        if random.random() < self.mutation_rate:
            self.mutate(final_child)
        return final_child

    # crossover operation
    # crossover([1,2,3,4], [10,20,30,40])
    # => [1, 20, 30, 40]
    def crossover(self, individual1, individual2):
        _tmp = random.randint(1, len(individual1))
        return individual1[:_tmp] + individual2[_tmp:]

    # Mutate an individual
    # Note that this method does a side effect. I.e., it does not create a new individual
    def mutate(self, an_individual):
        index = random.randrange(len(an_individual))
        an_individual[index] = self.gene_factory()
