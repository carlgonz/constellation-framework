import time
import json
import argparse
from random import randint
from datetime import datetime
import numpy as np
import pandas as pd
from numpy import rad2deg, deg2rad
from skyfield.api import EarthSatellite, Topos, load, utc
from sgp4.api import Satrec, WGS72
from sgp4.exporter import export_tle

# Base TLE Object
tle1_bsae = "1 42788U 17036Z   20274.92593008  .00002358  00000-0  10003-3 0  9992"
tle2_base = "2 42788  97.2915 326.6191 0011761  22.3632 337.8116 15.24022206181942"
satrec_base = Satrec.twoline2rv(tle1_bsae, tle2_base)

sgp4_base = [
    WGS72,                  # gravity model
    'i',                    # 'a' = old AFSPC mode, 'i' = improved mode
    satrec_base.satnum,     # satnum: Satellite number
    satrec_base.jdsatepoch-2433281.5,      # epoch: days since 1949 December 31 00:00 UT
    satrec_base.bstar,      # bstar: drag coefficient (/earth radii)
    satrec_base.ndot,       # ndot: ballistic coefficient (revs/day)
    satrec_base.nddot,      # nddot: second derivative of mean motion (revs/day^3)
    satrec_base.ecco,       # ecco: eccentricity
    satrec_base.argpo,      # argpo: argument of perigee (radians)
    satrec_base.inclo,      # inclo: inclination (radians)
    satrec_base.mo,         # mo: mean anomaly (radians)
    satrec_base.no_kozai,   # no_kozai: mean motion (radians/minute)
    satrec_base.nodeo,      # nodeo: right ascension of ascending node (radians)
]

base_scenario = {
    "id": -1,
    "start": -1,
    "duration": -1,
    "step": -1,
    "satellites": [
        # {"id": "1", "node": 1, "tle1": "1 45196U ...", "tle2": "2 45196  52.9956..."},
    ],
    "stations": [
        {"id": "stgo", "node": 4, "lat": -33.3833, "lon": -70.7833, "alt": 476},
        {"id": "tokyo", "node": 5, "lat": 35.6830, "lon": 139.7670, "alt": 5}
    ],
    "targets": [
        {"id": "saa", "node": 6, "lon": -15.0, "lat": -15, "alt": 0},
        {'id': 'AA', 'node': 20, 'lon': -150, 'lat': -60, 'alt': 0},
        {'id': 'AB', 'node': 21, 'lon': -150, 'lat': -30, 'alt': 0},
        {'id': 'AC', 'node': 22, 'lon': -150, 'lat': 0, 'alt': 0},
        {'id': 'AD', 'node': 23, 'lon': -150, 'lat': 30, 'alt': 0},
        {'id': 'AE', 'node': 24, 'lon': -150, 'lat': 60, 'alt': 0},
        {'id': 'AF', 'node': 25, 'lon': -120, 'lat': -60, 'alt': 0},
        {'id': 'AG', 'node': 26, 'lon': -120, 'lat': -30, 'alt': 0},
        {'id': 'AH', 'node': 27, 'lon': -120, 'lat': 0, 'alt': 0},
        {'id': 'AI', 'node': 28, 'lon': -120, 'lat': 30, 'alt': 0},
        {'id': 'AJ', 'node': 29, 'lon': -120, 'lat': 60, 'alt': 0},
        {'id': 'AK', 'node': 30, 'lon': -90, 'lat': -60, 'alt': 0},
        {'id': 'AL', 'node': 31, 'lon': -90, 'lat': -30, 'alt': 0},
        {'id': 'AM', 'node': 32, 'lon': -90, 'lat': 0, 'alt': 0},
        {'id': 'AN', 'node': 33, 'lon': -90, 'lat': 30, 'alt': 0},
        {'id': 'AO', 'node': 34, 'lon': -90, 'lat': 60, 'alt': 0},
        {'id': 'AP', 'node': 35, 'lon': -60, 'lat': -60, 'alt': 0},
        {'id': 'AQ', 'node': 36, 'lon': -60, 'lat': -30, 'alt': 0},
        {'id': 'AR', 'node': 37, 'lon': -60, 'lat': 0, 'alt': 0},
        {'id': 'AS', 'node': 38, 'lon': -60, 'lat': 30, 'alt': 0},
        {'id': 'AT', 'node': 39, 'lon': -60, 'lat': 60, 'alt': 0},
        {'id': 'AU', 'node': 40, 'lon': -30, 'lat': -60, 'alt': 0},
        {'id': 'AV', 'node': 41, 'lon': -30, 'lat': -30, 'alt': 0},
        {'id': 'AW', 'node': 42, 'lon': -30, 'lat': 0, 'alt': 0},
        {'id': 'AX', 'node': 43, 'lon': -30, 'lat': 30, 'alt': 0},
        {'id': 'AY', 'node': 44, 'lon': -30, 'lat': 60, 'alt': 0},
        {'id': 'AZ', 'node': 45, 'lon': 0, 'lat': -60, 'alt': 0},
        {'id': 'BA', 'node': 46, 'lon': 0, 'lat': -30, 'alt': 0},
        {'id': 'BB', 'node': 47, 'lon': 0, 'lat': 0, 'alt': 0},
        {'id': 'BC', 'node': 48, 'lon': 0, 'lat': 30, 'alt': 0},
        {'id': 'BD', 'node': 49, 'lon': 0, 'lat': 60, 'alt': 0},
        {'id': 'BE', 'node': 50, 'lon': 30, 'lat': -60, 'alt': 0},
        {'id': 'BF', 'node': 51, 'lon': 30, 'lat': -30, 'alt': 0},
        {'id': 'BG', 'node': 52, 'lon': 30, 'lat': 0, 'alt': 0},
        {'id': 'BH', 'node': 53, 'lon': 30, 'lat': 30, 'alt': 0},
        {'id': 'BI', 'node': 54, 'lon': 30, 'lat': 60, 'alt': 0},
        {'id': 'BJ', 'node': 55, 'lon': 60, 'lat': -60, 'alt': 0},
        {'id': 'BK', 'node': 56, 'lon': 60, 'lat': -30, 'alt': 0},
        {'id': 'BL', 'node': 57, 'lon': 60, 'lat': 0, 'alt': 0},
        {'id': 'BM', 'node': 58, 'lon': 60, 'lat': 30, 'alt': 0},
        {'id': 'BN', 'node': 59, 'lon': 60, 'lat': 60, 'alt': 0},
        {'id': 'BO', 'node': 60, 'lon': 90, 'lat': -60, 'alt': 0},
        {'id': 'BP', 'node': 61, 'lon': 90, 'lat': -30, 'alt': 0},
        {'id': 'BQ', 'node': 62, 'lon': 90, 'lat': 0, 'alt': 0},
        {'id': 'BR', 'node': 63, 'lon': 90, 'lat': 30, 'alt': 0},
        {'id': 'BS', 'node': 64, 'lon': 90, 'lat': 60, 'alt': 0},
        {'id': 'BT', 'node': 65, 'lon': 120, 'lat': -60, 'alt': 0},
        {'id': 'BU', 'node': 66, 'lon': 120, 'lat': -30, 'alt': 0},
        {'id': 'BV', 'node': 67, 'lon': 120, 'lat': 0, 'alt': 0},
        {'id': 'BW', 'node': 68, 'lon': 120, 'lat': 30, 'alt': 0},
        {'id': 'BX', 'node': 69, 'lon': 120, 'lat': 60, 'alt': 0},
        {'id': 'BY', 'node': 70, 'lon': 150, 'lat': -60, 'alt': 0},
        {'id': 'BZ', 'node': 71, 'lon': 150, 'lat': -30, 'alt': 0},
        {'id': 'CA', 'node': 72, 'lon': 150, 'lat': 0, 'alt': 0},
        {'id': 'CB', 'node': 73, 'lon': 150, 'lat': 30, 'alt': 0},
        {'id': 'CC', 'node': 74, 'lon': 150, 'lat': 60, 'alt': 0},
        {'id': 'saa-00', 'node': 100, 'lon': -75, 'lat': -45, 'alt': 0},
        {'id': 'saa-01', 'node': 101, 'lon': -75, 'lat': -30, 'alt': 0},
        {'id': 'saa-02', 'node': 102, 'lon': -75, 'lat': -15, 'alt': 0},
        {'id': 'saa-03', 'node': 103, 'lon': -75, 'lat': 0, 'alt': 0},
        {'id': 'saa-04', 'node': 104, 'lon': -60, 'lat': -45, 'alt': 0},
        {'id': 'saa-05', 'node': 105, 'lon': -60, 'lat': -30, 'alt': 0},
        {'id': 'saa-06', 'node': 106, 'lon': -60, 'lat': -15, 'alt': 0},
        {'id': 'saa-07', 'node': 107, 'lon': -60, 'lat': 0, 'alt': 0},
        {'id': 'saa-08', 'node': 108, 'lon': -45, 'lat': -45, 'alt': 0},
        {'id': 'saa-09', 'node': 109, 'lon': -45, 'lat': -30, 'alt': 0},
        {'id': 'saa-10', 'node': 110, 'lon': -45, 'lat': -15, 'alt': 0},
        {'id': 'saa-11', 'node': 111, 'lon': -45, 'lat': 0, 'alt': 0},
        {'id': 'saa-12', 'node': 112, 'lon': -30, 'lat': -45, 'alt': 0},
        {'id': 'saa-13', 'node': 113, 'lon': -30, 'lat': -30, 'alt': 0},
        {'id': 'saa-14', 'node': 114, 'lon': -30, 'lat': -15, 'alt': 0},
        {'id': 'saa-15', 'node': 115, 'lon': -30, 'lat': 0, 'alt': 0},
        {'id': 'saa-16', 'node': 116, 'lon': -15, 'lat': -45, 'alt': 0},
        {'id': 'saa-17', 'node': 117, 'lon': -15, 'lat': -30, 'alt': 0},
        {'id': 'saa-18', 'node': 118, 'lon': -15, 'lat': -15, 'alt': 0},
        {'id': 'saa-19', 'node': 119, 'lon': -15, 'lat': 0, 'alt': 0},
        {'id': 'saa-20', 'node': 120, 'lon': 0, 'lat': -45, 'alt': 0},
        {'id': 'saa-21', 'node': 121, 'lon': 0, 'lat': -30, 'alt': 0},
        {'id': 'saa-22', 'node': 122, 'lon': 0, 'lat': -15, 'alt': 0},
        {'id': 'saa-23', 'node': 123, 'lon': 0, 'lat': 0, 'alt': 0}
    ],
    "tracks": None,
    "contacts": None
}


def scenario_adhoc(nsats):
    """
    Create a list of dictionaries with satellite id, node and tle lines.
    Satellite TLE follows an Ad-hoc configuration with random altitude, inclination and separation parameters

    :param nsats: Number of satellites
    :return: List. [{"id": "1", "node": 1, "tle1": "1 45196U ...", "tle2": "2 45196  52.9956..."}, ...]
    """
    # Set inclination
    inclos = np.deg2rad(np.random.randint(80, 100, nsats))
    # Set separation
    mos = np.deg2rad(np.random.randint(0, 180, nsats))
    # Set right ascension of ascending node
    nodeo = np.deg2rad(np.random.randint(0, 180, nsats))
    # Generate random orbit altitude
    mu = 3.986044418e14
    earth_radio = 6371e3  # m
    alts = np.random.uniform(500e3, 600e3, nsats)  # m
    a = alts + earth_radio  # semi-major axis or mean distance
    n = np.sqrt(mu / a ** 3)*60  # rad/min

    # Create TLE objects
    satellites = []
    for i in range(nsats):
        sgp4_vars = sgp4_base.copy()
        sgp4_vars[9] = inclos[i]
        sgp4_vars[10] = mos[i]
        sgp4_vars[11] = n[i]
        sgp4_vars[12] = nodeo[i]
        satrec = satrec_base
        satrec.sgp4init(*sgp4_vars)
        tle1, tle2 = export_tle(satrec)
        satellites.append({"id": str(i), "node": i, "tle1": tle1, "tle2": tle2})

    return satellites


def scenario_walker(nsats, nplanes):
    """
    Create a list of dictionaries with satellite id, node and tle lines.
    Satellite TLE follows a walker configuration with nplanes and nsats//nplanes satellites
    equally spaced

    :param nsats: Number of satellites. This value is truncated if nsats%nplanes > 0
    :param nplanes: Number of planes
    :return: List. [{"id": "1", "node": 1, "tle1": "1 45196U ...", "tle2": "2 45196  52.9956..."}, ...]
    """
    nsats_by_plane = nsats//nplanes
    nsats = nsats_by_plane * nplanes  # In case nsats%nplanes > 0
    # Set inclination
    inclos = np.deg2rad(97 * np.ones(nsats))
    # Set separation
    mos = list(np.deg2rad(np.linspace(0, 180 - 180 / nsats_by_plane, nsats_by_plane))) * nplanes
    # Set right ascension of ascending node
    nodeo = sorted(list(np.deg2rad(np.linspace(0, 180 - 180 / nplanes, nplanes))) * nsats_by_plane)
    # Generate random orbit altitude
    mu = 3.986044418e14
    earth_radio = 6371e3  # m
    alts = np.array([500e3] * nsats)  # m
    a = alts + earth_radio  # semi-major axis or mean distance
    n = np.sqrt(mu / a ** 3)  # rad/s

    # Create TLE objects
    satellites = []
    for i in range(nsats):
        sgp4_vars = sgp4_base.copy()
        sgp4_vars[9] = inclos[i]
        sgp4_vars[10] = mos[i]
        sgp4_vars[11] = n[i] * 60  # rad/min
        sgp4_vars[12] = nodeo[i]
        satrec = satrec_base
        satrec.sgp4init(*sgp4_vars)
        tle1, tle2 = export_tle(satrec)
        satellites.append({"id": str(i), "node": i, "tle1": tle1, "tle2": tle2})

    return satellites


def get_parameters():
    """ Parse command line parameters """
    parser = argparse.ArgumentParser()

    parser.add_argument("type", metavar="TASK", default='adhoc', choices=["adhoc", "walker"], help="Constellation type")
    parser.add_argument("sats", metavar="SATS", type=int, help="Number of satellites")
    parser.add_argument("-p", "--planes", type=int, default=10, help="Number of planes (only if type=walker)")
    parser.add_argument("-o", "--output", default=None, help="Output file")
    parser.add_argument("-i", "--id", default=int(time.time()), type=int, help="Scenario ID")
    parser.add_argument("-s", "--start", default=None, type=int, help="Start time (unix timestamp)")
    parser.add_argument("-d", "--duration", default=10800, type=int, help="Simulation duration (seconds)")
    parser.add_argument("--step", default=30, type=int, help="Simulation step time (seconds)")

    return parser.parse_args()


if __name__ == "__main__":
    args = get_parameters()
    satellites = []
    if args.type == "adhoc":
        satellites = scenario_adhoc(args.sats)
    elif args.type == "walker":
        satellites = scenario_walker(args.sats, args.planes)

    jdepoch = satrec_base.jdsatepoch + satrec_base.jdsatepochF
    start_time = int((jdepoch - 2440587.5) * 86400)  # Julian date to unix timestamp

    # Add satellite list
    scenario = base_scenario.copy()
    scenario["satellites"] = satellites
    # Update scenario timing info
    scenario["id"] = args.id
    scenario["start"] = args.start if args.start is not None else start_time
    scenario["duration"] = args.duration
    scenario["step"] = args.step
    # Update stations and target nodes
    i = args.sats
    for sta in scenario["stations"]:
        sta["node"] = i
        i += 1
    for tgt in scenario["targets"]:
        tgt["node"] = i
        i += 1

    if args.output:
        with open(args.output, 'w') as jsonfile:
            json.dump(scenario, jsonfile, indent=4)
    else:
        print(json.dumps(scenario, indent=4, ensure_ascii=True))