#!/usr/bin/env python3
"""
Constellation Control Framework
--------------------------------
Framework entry point

:Date: 2021-11
:Version: 2
:Author: Carlos Gonzalez C. carlgonz@uchile.cl
"""
import os.path
import time
from contacts import generate_contact_list
from cpd import GeneticCPD
from plot_results import *
from simulator import run_simulation


class ConstellationController(object):
    def __init__(self, scenario, task=None):
        self.scenario = scenario
        self.task = task
        self.contact_list = None
        self.contact_plan = None
        self.flight_plan = None
        self._log_names = ["time", "solution", "fitness", "duration"]
        self._log = []

    def create_contact_list(self, base_fname=None, contacts_dt=120):
        """
        Generate the contact list of the scenario
        :param track_fname: String. Tracks filename
        :param contacts_fname: String. Contact list filename
        :return: DataFrame. Contact list
        """
        if not self.scenario:
            raise AttributeError("Scenario not initialized!")

        start = self.scenario.start
        sim_time = self.scenario.duration
        tracks_dt = self.scenario.step

        tracks, contacts = generate_contact_list(self.scenario, start, sim_time, tracks_dt, contacts_dt, base_fname)
        self.contact_list = contacts

        return tracks, contacts

    def create_contact_plan(self, population=50, mutation=0.3, iter=100, contact_fname=None, plot=False):
        """
        Creates the contact plan using the evolutive contact plan design (CPD)
        :return: DataFrame
        """
        # Genetic Contact Plan Design
        cpd = GeneticCPD(self.contact_list, self.scenario, self.task, population, mutation, iter, hops=4, silent=False)
        self.contact_plan, fitness, duration, converges = cpd.run()
        # Save log variables
        # ["val", "dt", "l", "time", "solution", "len"]
        self._log.append((cpd._time, cpd.best_individual.sequence, fitness, duration))

        # Save results
        contact_fname = time.strftime("%Y%m%d%H%M%S") + "_contact_plan.csv" if contact_fname is None else contact_fname
        self.contact_plan.to_csv(contact_fname, index=False)
        if plot:
            cpd.plot_results(self.scenario)
        return converges, self.contact_plan

    def check_contact_plan(self):
        """
        Check contact plan validity against rules R1-R5
        :return: Bool
        """
        try:
            # Check start and end
            assert self.contact_plan.iloc[0][COL_FROM] == self.scenario.get(self.task.start).node
            assert self.contact_plan.iloc[-1][COL_TO] == self.scenario.get(self.task.end).node
            # Check targets
            to_nodes = self.contact_plan.iloc[1:-1][COL_TO].to_list()
            for tgt in self.task.targets:
                tgt_node = self.scenario.get(tgt.id).node
                if tgt_node not in to_nodes:
                    assert(tgt.prio <= 0)
                else:
                    to_nodes.remove(tgt_node)
            # Check contacts
            tgt_nodes = [self.scenario.get(tgt.id).node for tgt in self.task.targets]
            for i in range(len(self.contact_plan)-1):
                if self.contact_plan.iloc[i][COL_TO] not in tgt_nodes:
                    assert(self.contact_plan.iloc[i+1][COL_FROM] == self.contact_plan.iloc[i][COL_TO])
                else:
                    assert(self.contact_plan.iloc[i+1][COL_FROM] == self.contact_plan.iloc[i][COL_FROM])
        except AssertionError:
            print("Invalid sequence")
            return False
        print("Valid sequence")
        return True

    def create_flight_plan(self, plan_fname=None):
        """
        Generate a Flight Plan table from the contact plan table,
        task and scenario definitions.
        :param contact_plan: DataFrame. Contact plan table
        :param task: Task. Task definition
        :param scenario: Scenario. Scenario definition
        :return: DataFrame. Flight plan table
        """
        if self.task is None or self.contact_plan is None:
            raise AttributeError("Task or contact plan not initialized!")

        sat_sta_nodes = [sat.node for sat in self.scenario.satellites + self.scenario.stations]
        sta_nodes = [sat.node for sat in self.scenario.stations]
        # Build {node: [target1, target2, ...]} structure
        target_nodes = {}
        for tgt in self.task.targets:
            tgt_node = self.scenario.get(tgt.id).node
            if tgt_node in target_nodes:
                target_nodes[tgt_node].append(tgt)
            else:
                target_nodes[tgt_node] = [tgt]

        data = []
        flight_plan = []

        for i, row in self.contact_plan.iterrows():
            time = row[COL_START]
            fp_entry = {"time": time, "node": row[COL_FROM], "command": None}

            # First check target, then ground stations (in case a gnd is used as target)
            # Case: target SAT->TGT use 'task target command'
            if row[COL_TO] in target_nodes.keys():
                to_node = row[COL_TO]
                try:
                    # Get, and consume (pop), one task target
                    tgt = target_nodes[to_node].pop(0)
                    fp_entry["command"] = tgt.command
                    flight_plan.append(fp_entry.copy())
                    # DATA <- data
                    data.append(tgt.result)
                except IndexError:
                    # No more targets
                    del target_nodes[to_node]
            # Case: X->SAT|GND use 'fp_send NODE'
            elif row[COL_TO] in sat_sta_nodes:
                # Transfer data if required use 'send_data'
                for d in data:
                    fp_entry["time"] = time
                    fp_entry["command"] = "sim_send_data {} {}".format(row[COL_TO], d)
                    flight_plan.append(fp_entry.copy())
                    time += 1
                # If data reaches a ground station, delete from queue
                if row[COL_TO] in sta_nodes:
                    data = []
                fp_entry["time"] = time
                fp_entry["command"] = "sim_send_fp {}".format(row[COL_TO])
                flight_plan.append(fp_entry.copy())

        self.flight_plan = pd.DataFrame(flight_plan)
        print(self.flight_plan.to_string())
        # Save results
        plan_fname = "logs/" + time.strftime("%Y%m%d%H%M%S") + "_flight_plan.csv" if plan_fname is None else plan_fname
        self.flight_plan.to_csv(plan_fname, index=False)
        self.task.solution = plan_fname
        return self.flight_plan


def main(working_path, scenario_path, task_path, solutions=1, size=50, mut=0.3, iter=500, tty=1, dt=120):
    """
    Constellation control framework entry point
    :param working_path: Str. Current working directory path
    :param scenario_path: Str. Path to scenario definition json, absolute or relative to @working_path
    :param task_path: Str. Path to task definition json, absolute or relative to @working_path
    :param solutions:Int. Genetic contact plan design max number of tries
    :param size: Int. Size of the GA population
    :param mut: Float. GA mutation rate
    :param iter: Int. GA max. iterations
    :return: None
    """
    print("Moving to:", working_path)
    prev_cwd = os.getcwd()
    os.chdir(working_path)

    # Load scenario and task definition
    with open(scenario_path) as scenario_file:
        scenario_json = json.load(scenario_file)
    with open(task_path) as task_file:
        task_json = json.load(task_file)

    scenario_name = os.path.basename(scenario_path).rsplit('.', 1)[0]
    task_name = os.path.basename(task_path).rsplit('.', 1)[0]

    scenario = Scenario(scenario_json)
    task = Task(task_json)
    controller = ConstellationController(scenario, task)

    # Generate or load tracks
    if controller.scenario.tracks is None or controller.scenario.contacts is None:
        track_filename = "{}_tracks.csv".format(scenario_name)
        contacts_filename = "{}_contacts.csv".format(scenario_name)
        if not os.path.exists("logs/"):
            os.mkdir("logs/")
        tracks, contacts = controller.create_contact_list("logs/"+scenario_name, dt)
    else:
        tracks, contacts = pd.read_csv(scenario.tracks), pd.read_csv(scenario.contacts)
        controller.contact_list = contacts

    # Generate or load contact plan
    if task.solution is None:
        for i in range(solutions):
            print("-------- SOLUTION {:02} --------".format(i))
            contact_plan_fname = "{}_{}_contact_plan_{}.csv".format(scenario_name, task_name, i)
            flight_plan_fname = "{}_{}_flight_plan_{}.csv".format(scenario_name, task_name, i)
            figname = "{}_{}_contact_plan_{}.png".format(scenario_name, task_name, i)
            converges, contact_plan = controller.create_contact_plan(size, mut, iter, contact_plan_fname, plot=False)
            plot_contact_list(contacts, scenario, contact_plan, figname=figname, show=False)
            valid = controller.check_contact_plan()
            if converges and valid:
                controller.create_flight_plan(flight_plan_fname)
                break
            else:
                mut += 0.1
                mut = mut % 1.0
                size += 50
    else:
        solution = pd.read_csv(task.solution)
        controller.flight_plan = solution

    # Run simulation
    tm_cmds, tm_status = run_simulation(task, scenario, controller.flight_plan, tty=tty, basedir="logs")
    #tm_cmds, tm_status = pd.read_csv("scenario_sim_tm_cmds.csv"), pd.read_csv("scenario_sim_tm_status.csv")
    # Sort and save telemetry
    print("Calculating lat long alt...")
    from telemetry import get_lonlatalt
    tm_status = get_lonlatalt(tm_status)
    print("Saving telemetry data...")
    tm_status = tm_status.sort_values(["timestamp"])
    tm_cmds = tm_cmds.sort_values(["timestamp"])
    tm_cmds.to_csv("{}_{}_tm_cmds.csv".format(scenario_name, task_name), index=False)
    tm_status.to_csv("{}_{}_tm_status.csv".format(scenario_name, task_name), index=False)

    print("Finish!")
    os.chdir(prev_cwd)


def get_parameters():
    """
    Parse command line parameters
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("dir", metavar='DIRECTORY', help="Project directory")
    parser.add_argument("scenario", metavar='SCENARIO', help="File with access list")
    parser.add_argument("task", metavar='TASK', help="Task file")
    parser.add_argument("-s", "--size", default=20, type=int, help="Population size")
    parser.add_argument("-m", "--mut", default=0.01, type=float, help="Mutation rate")
    parser.add_argument("-i", "--iter", default=50000, type=int, help="Max. iterations")
    parser.add_argument("-n", "--runs", default=1, type=int, help="Max number of solutions")
    parser.add_argument("-t", "--tty", default=-1, type=int, help="First tty to connect (-1 log to file)")
    parser.add_argument("-d", "--dt", default=120, type=int, help="Contacts dt")
    return parser.parse_args()


if __name__ == '__main__':
    args = get_parameters()
    main(args.dir, args.scenario, args.task, args.runs, args.size, args.mut, args.iter, args.tty, args.dt)
