# SUCHAI Constellation Framework

## Installation

1. Install python dependencies and SUCHAI flight software dependencies
   1. See next section
2. Clone this repository
   1. `git clone https://gitlab.com/carlgonz/constellation-framework.git`
3. Init SUCHAI constellation simulator and SUCHAI flight software repositories
   1. `sh init.sh`
    
### Dependencies

#### Python dependencies

Install the following dependencies with pip or your system package manager

- numpy
- pandas
- matplotlib
- cartopy (requires geos, proj)
- skyfield
- pyorbital

#### SUCHAI Flight Software dependencies

Install the following dependencies with your system package manager. 
More details in the [SUCHAI flight software repository.](https://gitlab.com/spel-uchile/suchai-flight-software/-/tree/feature/framework)

| Library name            | Ubuntu and family | Archlinux and family  |
| ----------------------- | --------------- | ------------------------|
| cmake >= 3.16           | cmake           | cmake                   |
| gcc >= 7.5              | gcc             | gcc                     |
| make >= 4.1             | make            | make                    |
| python2 >= 2.7.17       | python          | python2                 |
| zmq >= 4.2.5            | libzmq3-dev     | zeromq                  |
| pkg-config >= 0.29.1    | pkg-config      | pkgconf                 |
| (opt) sqlite >= 3.22    | libsqlite3-dev  | sqlite                  |
| (opt) libpq >= 10.17    | libpq-dev       | libpq-dev               |
| (opt) cunit >= 2.1.3    | libcunit1-dev   | postgresql              |

#### Cartopy
In Manjaro (Arch Linux) Install cartopy and dependencies via AUR (https://aur.archlinux.org/packages/python-cartopy/)

```shell
pamac build python-cartopy
yaourt -S python-cartopy
```

## Execute

Once all dependencies ar installed and repositories initialized (see section above) you can run a test case.

```shell
python3 controller.py cases/scenario_sim scenario_test.json task1.json -s 200 -m 0.8 -i 50
```

The following files with results are created

```
scenario_test_task1_contact_plan_0.csv
scenario_test_task1_contact_plan_0.png
scenario_test_task1_flight_plan_0.csv
scenario_test_task1_tm_cmds.csv
scenario_test_task1_tm_status.csv
```

Type `python3 controller.py --help` to see additional parameters

## Contact

Carlos Gonzalez Cortes, Ph. D.(c) Electrical Engineering

carlgonz[at]uchile.cl

### Cite

```
C. E. Gonzalez, A. Bergel and M. Diaz, Nanosatellite constellation control framework
using evolutionary contact plan designs Conference Paper. Accepted for presentation in
the 8th IEEE International Conference on Space Mission Challenges for Information
Technology (SMC-IT), 2021.
```