#!/usr/bin/env python3

import time
import glob
import argparse
import numpy as np
import pandas as pd
from datetime import datetime
from skyfield.api import EarthSatellite, Topos, load, utc
from definitions import *
from multiprocessing import Pool
from functools import partial


def interlink_map(sat, sats, index, dt, max_dist=3000):
    """
    Calc inter-satellite links.
    :param sat: Sat id.
    :param sats: Satellite definition list.
    :param index: List. Pandas MultiIndex converted to a list MultiIndex.tolist().
    :param dt: Dict with timestamp to TimeScale.
    :param max_dist: Maximum distance to consider a valid link
    :return: List of Dict.
        [{sat: Bool}, {sat: Bool}, .... {sat: Bool}]
    """
    print("\tCalculating sat: {}".format(sat))
    ts = load.timescale()
    sats = {sat.node: EarthSatellite(sat.tle1, sat.tle2, sat.id, ts) for sat in sats}
    return [{sat: 0 < (sats[sat] - sats[node]).at(dt[t]).distance().km < max_dist} if node != sat else {sat: False} for node, t in index]


def interlink_map_file(sat, sats, index, dt, max_dist=3000, fname=None):
    """
    Calc inter-satellite links. Operate with files.
    :param sat: Sat id.
    :param sats: Satellite definition list.
    :param index: List. Pandas MultiIndex converted to a list MultiIndex.tolist().
    :param dt: Dict with timestamp to TimeScale.
    :param max_dist: Maximum distance to consider a valid link
    :param fname: Str. Base filename to store intermediate result.
    :return: Created file name. Example:
        node,time,3
        0,1601424000,False
        0,1601424030,False
        0,1601424060,False
    """
    isl_contacts = interlink_map(sat, sats, index, dt, max_dist)
    index = pd.MultiIndex.from_tuples(index, names=['node', 'time'])
    isl_contacts = pd.DataFrame(isl_contacts, index=index)
    fname = '{}_contacts_tmp_{}.csv'.format(fname, sat)
    isl_contacts.to_csv(fname)

    return fname


def interlink_target_map(tgt, sats, index, dt, min_elev=5):
    """
    Calc satellite to ground station or target links.
    :param tgt: Target definition.
    :param sats: Satellite definition list.
    :param index: List. Pandas MultiIndex converted to a list MultiIndex.tolist().
    :param dt: Dict with timestamp to TimeScale.
    :param min_elev: Min elevation to consider a contact
    :return: List of Dict.
        [{tgt: Bool}, {tgt: Bool}, .... {tgt: Bool}]

        Equivalent to:
        gs_visible = lambda sat, t: {name: (sat - station).at(t).altaz()[0].degrees > 5 for name, station in stations.items()}
        gs_contacts = [gs_visible(sats[s], sim_dt[t]) for s, t in mi2]
    """
    print("\tCalculating target: {}".format(tgt.node))
    ts = load.timescale()
    target = Topos(latitude_degrees=tgt.lat, longitude_degrees=tgt.lon, elevation_m=tgt.alt)
    sats = {sat.node: EarthSatellite(sat.tle1, sat.tle2, sat.id, ts) for sat in sats}
    return [{tgt.node: (sats[node] - target).at(dt[t]).altaz()[0].degrees > min_elev} for node, t in index]


def interlink_target_map_file(tgt, sats, index, dt, min_elev=5, fname=None):
    """
    Calc inter-satellite links. Operate with files.
    :param tgt: Target definition
    :param sats: Satellite definition list.
    :param index: List. Pandas MultiIndex converted to a list MultiIndex.tolist().
    :param dt: Dict with timestamp to TimeScale.
    :param min_elev: Min elevation to consider a contact
    :param fname: Str. Base filename to store intermediate result.
    :return: Created file name. Example:
        node,time,3
        0,1601424000,False
        0,1601424030,False
        0,1601424060,False
    """
    tgt_contacts = interlink_target_map(tgt, sats, index, dt, min_elev)
    index = pd.MultiIndex.from_tuples(index, names=['node', 'time'])
    tgt_contacts = pd.DataFrame(tgt_contacts, index=index)
    fname = '{}_contacts_tmp_{}.csv'.format(fname, tgt.node)
    tgt_contacts.to_csv(fname)

    return fname


def calc_contacts(df):
    """
    Cal satellite contacts.
    :param df: DataFrame with inter-satellite links (Created with interlink_map function)
    :return: DataFrame with satellite contacts. Example:
         ,from,to,start,end,duration
        0,0,177,1601424030,1601467170,43140
        2,1,177,1601424030,1601467170,43140
        4,11,177,1601424030,1601456520,32490
        6,14,177,1601424030,1601467170,43140
    """
    l = []  # Store results here
    sat = df.columns[0]  # Sat ID
    print("\tContacts sat: {}".format(sat))

    def fix_start_end(df):
        """ Mark first and last line as false (invariant)"""
        df.iloc[-1] = False
        df.iloc[0] = False
        return df

    def create_contact(x: pd.DataFrame):
        """ Create a contact entry with from, to, start, end, and duration fields """
        d = {"from": x.index[0][0],
             "to": int(sat),
             "start": x.index[0][1],
             "end": x.index[1][1],
             "duration": x.index[1][1]-x.index[0][1]
             }
        l.append(d)  # Append real result
        return x.sum()  # Dummy return, not used

    df2 = df.groupby('node').apply(fix_start_end)  # Fix start and end of sections
    df2 = df2.rolling(2).apply(lambda x: np.logical_xor(x[0], x[1]), raw=True)  # Mark start and end of a contact

    df2 = df2[df2[sat] == 1]
    df2.rolling(2).apply(create_contact)  # Create contact entries
    df3 = pd.DataFrame(l)
    df4 = df3.iloc[::2]  # Only keep not consecutive pairs (because of rolling)

    return df4


def calc_contacts_dt(df, dt):
    """
    Cal satellite contacts.
    :param df: DataFrame with inter-satellite links (Created with interlink_map function)
    :param dt: Int. Seconds to discretion the contact list.
    :return: DataFrame with satellite contacts. Example:
         ,from,to,start,end,duration
        0,0,177,1601424030,1601467170,43140
        2,1,177,1601424030,1601467170,43140
        4,11,177,1601424030,1601456520,32490
        6,14,177,1601424030,1601467170,43140
    """
    sat = df.columns[0]  # Sat ID
    print("\tContacts sat: {}".format(sat))

    times = pd.to_datetime(df.index.unique(level=1), unit='s')
    df1 = df.copy()
    df1.index = df1.index.set_levels(times, level=1)
    df2 = df1.groupby('node').resample('{}s'.format(dt), level=1).apply(lambda x: np.any(x))
    df2 = df2[df2[sat] == True]
    df2.reset_index(inplace=True)
    df2.rename(columns={"node": "from", "time": "start"}, inplace=True)
    df2['start'] = df2.start.view(np.int64) // 10**9
    df2['end'] = df2['start'] + dt
    df2['duration'] = dt
    df2['to'] = sat
    df2.sort_values('start', ignore_index=True, inplace=True)
    df2['access'] = df2.index
    df2.drop(columns=[sat], inplace=True)
    df2 = df2.reindex(["access", "from", "to", "start", "end", "duration"], axis=1)
    return df2


def cal_contacts_file(dfname, outfname, dt=None):
    """
    Cal satellite contacts. Operate with files
    :param dfname: Str. File name with inter-satellite links (Created with interlink_map_file function)
    :param outfname: Str. Output file name
    :param dt: Int. Seconds to discretion the contact list. None to omit and consider the total start-end time.
    :return: CSV file name with satellite contacts. Example:
         ,from,to,start,end,duration
        0,0,177,1601424030,1601467170,43140
        2,1,177,1601424030,1601467170,43140
        4,11,177,1601424030,1601456520,32490
        6,14,177,1601424030,1601467170,43140
    """
    try:  # Ok with numpy version 1.19.2, Pandas version 1.1.2
        df = pd.read_csv(dfname, index_col=["node", "time"])
    except TypeError:  # Fix error with numpy version 1.19.2, Pandas version 1.1.3
        df = pd.read_csv(dfname)
        df.set_index(["node", "time"], inplace=True)

    if dt is None:
        df4 = calc_contacts(df)
        outfname = '{}_isl_tmp_{}.csv'.format(outfname, df.columns[0])
    else:
        df4 = calc_contacts_dt(df, dt)
        outfname = '{}_isl_tmp_{}s_{}.csv'.format(outfname, dt, df.columns[0])

    df4.to_csv(outfname)
    return outfname


def generate_contact_list(scenario: Scenario, start: int, sim_time: int, sim_dt: int, dt: int, base_fname: str = None, calc_contacts=True, ncpu=None):
    """
    Generate the contact list of the scenario
    :param scenario: Scenario. Scenario definition object
    :param start: Int. Start time seconds (unix time)
    :param sim_time: Int. Duration (seconds since start)
    :param sim_dt: Int. Simulation resolution (seconds)
    :param dt: Int. Contact List resolution
    :param base_fname: Str. Tracks filename
    :param contacts_fname: Str. Contact list filename
    :return: [DataFrame, DataFrame]. Tracks and contact list datafiles
    """
    base_fname = time.strftime("%Y%m%d%H%M%S") if base_fname is None else base_fname
    track_fname = "{}_tracks.csv".format(base_fname)

    ti = time.time()
    times = list(range(start, start + sim_time, sim_dt))
    ts = load.timescale()

    # stations = {gs.node: Topos(latitude_degrees=gs.lat, longitude_degrees=gs.lon, elevation_m=gs.alt) for gs in
    #             scenario.stations}
    # targets = {tgt.node: Topos(latitude_degrees=tgt.lat, longitude_degrees=tgt.lon, elevation_m=tgt.alt) for tgt in
    #            scenario.targets}
    sats = {sat.node: EarthSatellite(sat.tle1, sat.tle2, sat.id, ts) for sat in scenario.satellites}

    sim_dt = {t: ts.utc(datetime.fromtimestamp(t, utc)) for t in times}
    mi2 = pd.MultiIndex.from_product([sats.keys(), sim_dt.keys()], names=['node', 'time'])

    if calc_contacts:
        # Calulate satellite positions
        print("Calculating satellite tracks...")
        latlot = lambda topos: {"lat": topos.latitude.degrees, "lon": topos.longitude.degrees, "alt": topos.elevation.m}
        pos = [latlot(sats[s].at(sim_dt[t]).subpoint()) for s, t in mi2]
        pos = pd.DataFrame(pos, index=mi2)

        # # Satellite to ground station contacts
        # print("Calculating ground station passes...")
        # gs_visible = lambda sat, t: {name: (sat - station).at(t).altaz()[0].degrees > 5 for name, station in
        #                              stations.items()}
        # gs_contacts = [gs_visible(sats[s], sim_dt[t]) for s, t in mi2]
        # gs_contacts = pd.DataFrame(gs_contacts, index=mi2)
        #
        # # Satellite to targets contacts
        # print("Calculating target contacts...")
        # tgt_visible = lambda sat, t: {name: (sat - target).at(t).altaz()[0].degrees > 5 for name, target in targets.items()}
        # tgt_contacts = [tgt_visible(sats[s], sim_dt[t]) for s, t in mi2]
        # tgt_contacts = pd.DataFrame(tgt_contacts, index=mi2)

        # Build tracks datafile
        print("Saving tracks...")
        # tracks = pos.join(gs_contacts).join(tgt_contacts)
        tracks = pos
        tracks.to_csv(track_fname)

        print("Calculating ground station and target contacts...")
        interlink_target_partial = partial(interlink_target_map_file, sats=scenario.satellites, index=mi2.tolist(), dt=sim_dt, min_elev=5, fname=base_fname)
        with Pool(ncpu) as pool:
            t0 = time.time()
            tgt_lists = pool.map(interlink_target_partial, scenario.stations + scenario.targets)
            t1 = time.time()
            print("\ttook {} s".format(t1 - t0))
            print(tgt_lists)

        # Satellite to satellite contacts
        print("Calculating satellite contacts...")
        interlink_partial = partial(interlink_map_file, sats=scenario.satellites, index=mi2.tolist(), dt=sim_dt, max_dist=3000, fname=base_fname)
        with Pool(ncpu) as pool:
            t0 = time.time()
            isl_lists = pool.map(interlink_partial, sats.keys())
            t1 = time.time()
            print("\ttook {} s".format(t1-t0))
            print(isl_lists)

        contacts_files = isl_lists + tgt_lists

    else:
        # Load contacts from file
        contacts_files = glob.glob(base_fname+"_contacts_tmp*")
        assert len(contacts_files) == len(scenario.satellites)+len(scenario.targets)+len(scenario.stations)
        assert os.path.isfile(track_fname)
        tracks = pd.read_csv(track_fname)

    # Build contacts list datafile
    contact_partial = partial(cal_contacts_file, outfname=base_fname, dt=dt)
    print("Building contact list datafile...")
    with Pool(ncpu) as pool:
        t0 = time.time()
        # Satellites and targets contact lists
        contact_lists = pool.map(contact_partial, contacts_files)
        # # Target and stations contact lists
        # tgt_contacts = [tracks[[col]] for col in tracks.columns[3:].tolist()]
        # tgt_lists = pool.map(calc_contacts, tgt_contacts)
        t1 = time.time()
        print("\ttook {} s".format(t1 - t0))
        print(contact_lists)

        print("\tmerging...")
        contacts = [pd.read_csv(fname, index_col=0) for fname in contact_lists]
        # contacts.extend(tgt_lists)
        contacts = pd.concat(contacts)
        # Fix unidirectional links between satellite and target
        n = sorted(sats.keys())[-1]
        c = contacts[contacts["to"] > n][:]
        c[["from", "to"]] = c[["to", "from"]]
        contacts = pd.concat([contacts, c], ignore_index=True)
        contacts = contacts.sort_values(COL_START, ignore_index=True)
        contacts = remove_duplicates(contacts)
        contacts = contacts.sort_values(COL_START, ignore_index=True)
        contacts['access'] = contacts.index
        contacts_fname = "{}_contacts_{}s.csv".format(base_fname, dt)
        contacts.to_csv(contacts_fname, index=False)

    tf = time.time()
    print("Finish! Took {} s ({} min)".format(tf-ti, (tf-ti)/60))
    return tracks, contacts


def remove_duplicates(df: pd.DataFrame):
    df1 = df.copy()
    df1['id'] = df1[['from', 'to']].apply(lambda x: "{}".format(sorted([x[0], x[1]])), axis=1)
    df2 = df1.groupby('start').apply(lambda x: x.groupby('id').first())
    df2 = df2.reset_index(drop=True)
    # df2.to_csv(filename, index=False)
    return df2


def get_parameters():
    """ Parse command line parameters """
    parser = argparse.ArgumentParser()
    parser.add_argument("scenario", metavar='SCENARIO', help="File with access list")
    parser.add_argument("-s", "--start", default=None, type=int, help="Starting time")
    parser.add_argument("-t", "--time", default=None, type=int, help="Simulation time")
    parser.add_argument("-d", "--sim_dt", default=None, type=int, help="Tracks time step")
    parser.add_argument("--dt", default=None, type=int, help="Contacts time step")
    parser.add_argument("-n", "--nsats", default=None, type=int, help="Number of satellites")
    parser.add_argument("-p", "--plot", action="store_true", help="Show plots")
    parser.add_argument("--load", action="store_false", help="Load precalculate contacts")
    return parser.parse_args()


def test_performance(scenario, start, duration, step, dt, basename):
    import os
    import time
    log_fname = "contacts_perf_{}.csv".format(time.time())
    with open(log_fname, 'w') as logfile:
        logfile.write("#satellites: {}\n".format(len(scenario.satellites)))
        logfile.write("#targets: {}\n".format(len(scenario.targets)+len(scenario.stations)))
        logfile.write("#dureation: {}\n".format(duration))
        logfile.write("#tracks_step: {}\n".format(step))
        logfile.write("#contact_step: {}\n".format(step))
        logfile.write("n_cpu,time\n")

        for n_cpu in range(1, os.cpu_count()+1):
            prev_dir = os.getcwd()
            new_dir = "{}".format(n_cpu)
            if not os.path.exists(new_dir+"/logs"):
                os.makedirs(new_dir+"/logs")
            os.chdir(new_dir)

            t_ini = time.time()
            tracks, contacts = generate_contact_list(scenario, start, duration, step, dt, base_fname=basename, calc_contacts=True, ncpu=n_cpu)
            t_fin = time.time()

            os.chdir(prev_dir)
            log = "{},{}\n".format(n_cpu, t_fin-t_ini)
            print(log)
            logfile.write(log)

    return tracks, contacts


if __name__ == "__main__":
    import os
    import json
    from plot_results import plot_tracks, plot_contact_list

    args = get_parameters()
    # Load scenario definition
    with open(args.scenario) as scenario_file:
        scenario_json = json.load(scenario_file)
    scenario = Scenario(scenario_json, max=args.nsats)

    start = args.start if args.start is not None else scenario.start
    duration = args.time if args.time is not None else scenario.duration
    step = args.sim_dt if args.sim_dt is not None else scenario.step

    prev_cwd = os.getcwd()
    new_cwd = os.path.dirname(args.scenario)
    os.chdir(new_cwd)
    if not os.path.exists("logs"):
        os.mkdir("logs")
    base_fname = os.path.join("logs/", os.path.basename(args.scenario).split('.')[0])
    tracks, contacts = generate_contact_list(scenario, start, duration, step, args.dt, base_fname=base_fname, calc_contacts=args.load)
    # tracks, contacts = test_performance(scenario, start, duration, step, args.dt, base_fname)
    if args.plot:
        plot_tracks(tracks.reset_index(), scenario)
        plot_contact_list(contacts, scenario, plot_duration=True)
    os.chdir(prev_cwd)
