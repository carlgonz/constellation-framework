#!/usr/bin/env bash
[ ! -d suchai-constellation-simulator ] && git clone -b framework https://gitlab.com/carlgonz/suchai-constellation-simulator.git
if [ -d suchai-constellation-simulator ]
then
    cd suchai-constellation-simulator
    sh init.sh
    sh build_simulator.sh
    cd -
else
    echo "Error: suchai-constellation-simulator directory does not exists"
fi
