#!/usr/bin/env python3
import os
import re
import sys
import glob
import time
import json
import signal
import argparse
import pandas as pd
import numpy as np
from subprocess import Popen, PIPE, DEVNULL

# from telemetry import *
from definitions import *

SCH_TRX_PORT_TM = 9   # Telemetry port
SCH_TRX_PORT_TC = 10  # Telecommands port
SCH_TRX_PORT_RPT = 11  # Digirepeater port (resend packets)
SCH_TRX_PORT_CMD = 12  # Commands port (execute console commands)
SCH_TRX_PORT_DBG = 13  # Debug port, logs output
COM_FRAME_MAX_LEN = 200  # Packet size (bytes)

FILE_PATH = os.path.dirname(os.path.abspath(__file__))
SCH_ZMQ_EXEC_PATH = os.path.join(FILE_PATH, "suchai-constellation-simulator/suchai-flight-software/sandbox/csp_zmq/zmqhub.py")
SCH_FS_EXEC_PATH = os.path.join(FILE_PATH, "suchai-constellation-simulator/build/apps/simulator/suchai-app")
SCH_FS_EXEC_PARAMS = {"-n": 0, "-i": 0, "-s": 0, "-e": 0, "-t": 0, "-l": 0}

sys.path.append(os.path.dirname(SCH_ZMQ_EXEC_PATH))
from zmqnode import CspZmqNode, CspHeader, threaded

class Simulator(CspZmqNode):
    def __init__(self, nodes, ttys, start_s, duration_s, step_ms, cwd="."):
        self.ttys = ttys
        self.nodes = nodes
        self.obcs = []
        self.hub = None
        self.timer = None
        self.start_s = start_s
        self.duration_s = duration_s
        self.step_ms = step_ms
        self.cwd = cwd

        self.tm_status = {node: [] for node in self.nodes}
        self.tm_commands = {node: [] for node in self.nodes}
        self._elapsed = {node: 0 for node in self.nodes}

        CspZmqNode.__init__(self, "31", writer=True)

    def join(self):
        self.hub.wait()
        CspZmqNode.join(self)

    def stop(self):
        self.timer.wait()
        for obc in self.obcs:
            obc.wait()
            print("OBC Closed!", obc.args)
        self.hub.kill()

        CspZmqNode.stop(self)

    def kill(self):
        self.timer.kill()
        for obc in self.obcs:
            obc.kill()
        self.hub.kill()

        CspZmqNode.stop(self)

    def start(self):
        # ZMQ HUB
        self.hub = Popen(["python3", SCH_ZMQ_EXEC_PATH], stdin=DEVNULL, stdout=DEVNULL)
        CspZmqNode.start(self)

        # NODES AND TIME CONTROLLER
        SCH_FS_EXEC_PARAMS["-n"] = -1
        SCH_FS_EXEC_PARAMS["-i"] = -1
        SCH_FS_EXEC_PARAMS["-s"] = self.start_s
        SCH_FS_EXEC_PARAMS["-e"] = self.duration_s
        SCH_FS_EXEC_PARAMS["-t"] = 1
        SCH_FS_EXEC_PARAMS["-l"] = len(self.nodes)
        PARAM_LIST = list(np.array(list(SCH_FS_EXEC_PARAMS.items())).flatten())
        self.timer = Popen([SCH_FS_EXEC_PATH]+PARAM_LIST, cwd=self.cwd, stdin=DEVNULL, stdout=self.ttys[-1], stderr=self.ttys[-1], preexec_fn=lambda: signal.signal(signal.SIGINT, signal.SIG_IGN))
        time.sleep(1) # Timer node must start first to initialize semaphores

        for node, tty in zip(self.nodes, self.ttys):
            SCH_FS_EXEC_PARAMS["-n"] = node
            SCH_FS_EXEC_PARAMS["-i"] = node
            SCH_FS_EXEC_PARAMS["-t"] = 0
            PARAM_LIST = list(np.array(list(SCH_FS_EXEC_PARAMS.items())).flatten())
            obc = Popen([SCH_FS_EXEC_PATH]+PARAM_LIST, cwd=self.cwd, stdin=DEVNULL, stdout=tty, stderr=tty, preexec_fn=lambda: signal.signal(signal.SIGINT, signal.SIG_IGN))
            self.obcs.append(obc)

    def send_command(self, command, nodes=None):
        if nodes is None:
            nodes = self.nodes

        for dnode in nodes:
            hdr = CspHeader(src_node=self.node, dst_node=dnode, dst_port=SCH_TRX_PORT_TC)
            self.send_message(command, hdr)

    def read_message(self, message, header=None):
        print(header.src_node, header.dst_port, message)


def run_simulation(task, scenario, fp, tty=0, ip="localhost", in_port="8001", out_port="8002", basedir="."):
    csp_nodes = [node.node for node in scenario.satellites+scenario.stations]
    nodes = list(csp_nodes)
    sim_start = fp["time"][0] - 20
    sim_end = fp["time"][len(fp)-1] + 10
    sim_duration = sim_end - sim_start
    sim_step = scenario.step

    if tty < 0:
        ttys = [open(os.path.join(basedir, "console_{}_{}.log".format(i, int(time.time()))), "w+") for i in nodes+[-1]]
    else:
        ttys = [open("/dev/pts/{}".format(i), "wb+", buffering=0) for i in range(tty, tty+len(nodes)+1)]

    simulator = Simulator(nodes, ttys, sim_start, sim_duration, sim_step, cwd=basedir)
    simulator.start()
    time.sleep(0.5)

    # Reset Repo data
    # simulator.send_command("drp_ebf 1010")

    # Set up TLE
    for sat in scenario.satellites:
        simulator.send_command("tle_set "+sat.tle1, [sat.node])
        simulator.send_command("tle_set "+sat.tle2, [sat.node])
        simulator.send_command("tle_update", [sat.node])

    # Set up task
    first_node = scenario.get(task.start).node
    for i, line in fp.iterrows():
        # sim_set_fp time node cmd args
        cmd = "sim_set_fp {} {} {}".format(line["time"], line["node"], line["command"])
        simulator.send_command(cmd, [first_node])

    # Run simulation
    try:
        # Wait until timer and obcs exit
        print("Simulation running")
        simulator.stop()
    except KeyboardInterrupt:
        pass

    print("Stopping simulation")
    simulator.kill()
    for console_file in ttys:
        console_file.close()

    # Merge logs and telemetry files
    print("Merging telemetry files...")
    tm_cmd_files = glob.glob(os.path.join(basedir, "tm_cmds_*.csv"))
    tm_status_files = glob.glob(os.path.join(basedir, "tm_status_*.csv"))

    tm_dfs = []
    for tm_files in [tm_cmd_files, tm_status_files]:
        tm_df = []
        for f in tm_files:
            df = pd.read_csv(f)
            df["sat"] = int(re.findall(r'.+_(\d+).csv', f)[0])
            tm_df.append(df)
        tm_dfs.append(pd.concat(tm_df, ignore_index=True))

    return tm_dfs


def get_parameters():
    """ Parse command line parameters """
    parser = argparse.ArgumentParser()

    parser.add_argument("scenario", metavar="SCENARIO", help="Path to scenario JSON")
    parser.add_argument("task", metavar="TASK", help="Path to task JSON")
    parser.add_argument("-t", "--tty", default=-1, type=int, help="First tty to connect")
    parser.add_argument("-d", "--ip", default="localhost", help="Hub IP address")
    parser.add_argument("-i", "--inport", default="8001", help="Input port")
    parser.add_argument("-o", "--outport", default="8002", help="Output port")

    return parser.parse_args()


if __name__ == "__main__":
    args = get_parameters()
    with open(args.task) as task_file:
        task_json = json.load(task_file)
    with open(args.scenario) as scenario_file:
        scenario_json = json.load(scenario_file)

    scenario = Scenario(scenario_json)
    task = Task(task_json)
    assert (task.solution is not None)
    flight_plan = pd.read_csv(task.solution)
    run_simulation(task, scenario, flight_plan, tty=args.tty, ip=args.ip, in_port=args.inport, out_port=args.outport)
