#!/usr/bin/env python3
"""
Constellation Control Framework
--------------------------------
Evolutive Contact Plan Design (Version 2)

:Date: 2021-11
:Version: 1.1
:Author: Carlos Gonzalez C. carlgonz@uchile.cl
"""
import matplotlib
# matplotlib.use('Agg')
import os
import time
import argparse
import numpy as np
import pandas as pd
from random import choice, choices, sample, randint, random
import matplotlib.pyplot as plt
from ga import GA
from definitions import *
from plot_results import plot_contact_list
from multiprocessing import Pool


class Individual(object):
    """
    An individual with some extra information
    """
    def __init__(self, seq: list, m: list, contacts=None):
        """
        Create a new individual
        :param seq: List of nodes to visit
        :param m: Index of the target node (do not modify in mutations or xover)
        :param contacts: List of indexes in the contact list
        """
        self.t_index = m
        self.sequence = seq
        self.contacts = contacts
        self.fitness = -1
        self.duration = np.inf

    def reset_fitness(self):
        self.fitness = -1
        self.duration = np.inf
        self.contacts = None

    def set_fitness(self, fitness, duration=np.inf, contacts=None):
        self.fitness = fitness
        self.duration = duration
        self.contacts = contacts

    def __len__(self):
        return len(self.sequence)

    def __repr__(self):
        return "T: {}; Seq: {}; Contacts: {}; Fitness {}/{}; Duration {}".format(
            self.t_index, self.sequence, self.contacts, self.fitness, len(self.sequence)-1, self.duration)


class GeneticCPD(GA):
    def __init__(self, contact_list: pd.DataFrame, scenario: Scenario, task: Task, pop_size: int, mutation_rate: float,
                 max_iter: int = 100, silent: bool = False, hops=2):
        """
        Contact plan design using genetic algorithm
        :param contact_list: DataFrame. Contact list table
        :param task_nodes: List. List of task target node numbers (start, <targets>, end)
        :param relays: List. List of node numbers that are relays (satellites and stations)
        :param targets: List. List of node numbers that are not relays (targets)
        :param pop_size: Int. Initial population size
        :param mutation_rate: Float. Mutation rate
        :param max_iter: Int. Max. number of iterations
        :param silent: Bool. Mute logs
        """
        # GA variables
        self.population_size = pop_size
        self.mutation_rate = mutation_rate
        self.max_iterations = max_iter
        self.population = list()
        self.silent = silent
        self.individual_fitnesses = list()
        self.best_index = None
        self.best_individual = None
        self.current_iteration = 0

        # Contact list and contact plan
        self.scenario = scenario
        self.task = task
        self.contact_list = contact_list.copy()
        self.contact_plan = None
        assert (hops > len(self.task.targets))
        self.hops = hops
        self.len = len(self.task.targets)*3 + hops + 2

        # Internal performance variables
        self._time = -1
        self._tmp_fit = list()
        self._best_fitness_list = list()
        self._avg_list = list()
        self._goal = list()
        # self._worst_list = list()
        # self._multi_fitness_list = list()
        # self._pareto = list()

    def individual_factory(self):
        """
        Creates a new individual: random list of nodes, random target position, fixed
        start and end nodes.
        :return: Individual
        """
        seq = choices(self.scenario._nodes, k=self.len)
        seq[0] = self.scenario.get(self.task.start).node
        seq[-1] = self.scenario.get(self.task.end).node
        s = len(seq) // len(self.task.targets)
        t_indexes = []
        for i in range(len(self.task.targets)):
            t_start = i*s+2
            t_end = (i+1)*s-3
            t_index = randint(t_start, t_end)
            seq[t_index] = self.scenario.get(self.task.targets[i].id).node
            seq[t_index+1] = seq[t_index-1]
            t_indexes.append(t_index)

        individual = Individual(seq, t_indexes)
        return individual

    def gene_factory(self):
        """
        Return a gene: a random node number
        :return: Int
        """
        return choice(self.scenario._nodes)

    def crossover(self, individual1: Individual, individual2: Individual):
        """
        Crossover operation. Do not alter the start, target and end nodes positions.
        Always return a new valid sequence.
        :param individual1: Individual
        :param individual2: Individual
        :return: Individual. Always return a new valid sequence.
        """
        seq1, ti1 = individual1.sequence, individual1.t_index
        seq2, ti2 = individual2.sequence, individual2.t_index
        if random() < 0.5:
            t_split = choice(ti1)
            seq = seq1[:t_split+2] + seq2[t_split+2:]
            ti = [t for t in ti1 if t <= t_split+2] + [t for t in ti2 if t > t_split+2]
        else:
            t_split = choice(ti2)
            seq = seq2[:t_split+2] + seq1[t_split+2:]
            ti = [t for t in ti2 if t <= t_split+2] + [t for t in ti1 if t > t_split+2]

        # Fix overlapped sequences
        if len(ti) > len(ti1):
            ti = choices(ti, k=len(ti1))
            # for t in ti:
            #     assert seq[t+1] == seq[t-1]

        an_individual = Individual(seq, ti)
        an_individual.reset_fitness()
        return an_individual

    def crossover_old(self, individual1: Individual, individual2: Individual):
        """
        Crossover operation. Do not alter the start, target and end nodes positions.
        Always return a new valid sequence.
        :param individual1: Individual
        :param individual2: Individual
        :return: Individual. Always return a new valid sequence.
        """
        individual1, m1 = individual1.sequence, individual1.t_index
        individual2, m2 = individual2.sequence, individual2.t_index
        i = randint(1, len(individual1)-2)
        if i < min([m1, m2])-1:
            _i = i+1
            individual = individual1[:_i] + individual2[_i:]
            m = m2
        elif min([m1, m2])-1 <= i <= max([m1, m2])+1:
            _i = max([m1, m2])+2
            individual = individual1[:_i] + individual2[_i:]
            # TODO: case i > m -> individual2[:i] + individual1[i:]
            m = m1
        else:
            _i = i
            individual = individual1[:_i] + individual2[_i:]
            m = m1

        an_individual = Individual(individual, m)
        an_individual.reset_fitness()
        return an_individual

    def mutate(self, an_individual: Individual, mut: float = 0.3):
        """
        Mutate an individual.
        Does not alter the start, target and end nodes positions. Modifies node before
        and after the target if necessary so always creates a valid sequence.
        Note that this method does a side effect. I.e., it does not create a new individual
        :param an_individual: Individual to mutate; the object is modified by the method
        :param mut: Mutation rate in the sequence
        :return: None
        """
        individual, t_index = an_individual.sequence, an_individual.t_index

        for index in range(1, len(individual)-1):
            if random() > mut:
                continue

            # index = randint(1, len(individual)-2)
            g = self.gene_factory()

            # Do not mutate the target
            if index in t_index: #  or g in [individual[t] for t in t_index]:
                s = len(individual) // len(self.task.targets)
                i = index // s
                t_start = i * s + 2
                t_end = (i + 1) * s - 3
                d_index = randint(t_start, t_end)-1
                o_index = index-1
                dst = individual[d_index:d_index+3].copy()
                ori = individual[o_index:o_index+3].copy()
                individual[d_index:d_index+3] = ori
                individual[o_index:o_index+3] = dst[::-1]
                t_index.remove(index)
                t_index.append(d_index+1)
                an_individual.t_index = t_index
            elif index+1 in t_index:
                individual[index] = g
                individual[index+2] = g
            elif index-1 in t_index:
                individual[index] = g
                individual[index-2] = g
            else:
                individual[index] = g

        an_individual.sequence = individual
        an_individual.reset_fitness()

    def mutate_single(self, an_individual: Individual):
        """
        Mutate an individual.
        Does not alter the start, target and end nodes positions. Modifies node before
        and after the target if necessary so always creates a valid sequence.
        Note that this method does a side effect. I.e., it does not create a new individual
        :param an_individual: Individual to mutate; the object is modified by the method
        :return: None
        """
        individual, t_index = an_individual.sequence, an_individual.t_index
        index = randint(1, len(individual)-2)
        g = self.gene_factory()

        # Do not mutate the target
        if index in t_index: #  or g in [individual[t] for t in t_index]:
            return
        elif index+1 in t_index:
            individual[index] = g
            individual[index+2] = g
        elif index-1 in t_index:
            individual[index] = g
            individual[index-2] = g
        else:
            individual[index] = g

        an_individual.sequence = individual
        an_individual.reset_fitness()

    @staticmethod
    def _sort_population(population):
        return sorted(enumerate(population), key=lambda ind: (ind[1].fitness, -ind[1].duration), reverse=True)

    # return the best individual of the current population
    def get_best_individual(self):
        sorted_pop = self._sort_population(self.population)
        index, individual = sorted_pop[0]
        return index, individual

    # Obtain the best individual from a tournament on the population
    def tournament(self, k=10):
        competitors = sample(self.population, k=k)
        competitors = self._sort_population(competitors)
        index, individual = competitors[0]
        return individual

    def termination_condition(self, fitness):
        if fitness == self.len-1:
            self._goal.append(self.best_individual.duration)

        if len(self._goal) > 5:
            return np.mean(self._goal[-5:]) == self._goal[-1]
        else:
            return False

    def fitness_function(self, individual: Individual):
        # return self.fitness_function_lineal(individual)
        # return self.fitness_function_partial(individual)
        return self.fitness_function_slotted(individual)

    def fitness_function_slotted(self, individual: Individual):
        """
        Evaluate fitness function

        :param individual: List. Individual to evaluate
        :return: Float.
        """
        sequence = individual.sequence
        contacts = [(sequence[i], sequence[i+1]) for i in range(len(sequence)-1)]
        result = []

        contact_list = self.contact_list.set_index([COL_FROM, COL_TO])

        # Evaluate the proposed contact list (find if the proposed contacts exist)
        i = 0
        for _, slot in contact_list.groupby(COL_START):
            if i < len(contacts):
                contact = contacts[i]
                # The next hop could be the last contact in the opposite direction
                if i > 0:
                    row = self.contact_list.iloc[result[-1]]
                    if row[COL_FROM] in contact and row[COL_TO] in contact:
                        result.append(row[COL_ACCESS])
                        i += 1
                        continue
                # Check if the contact exists in this time slot
                try:
                    # Test [A->B] Exists
                    row = slot.loc[contact]
                    result.append(row[COL_ACCESS])
                    i += 1
                except KeyError:
                    try:
                        # Test [B->A] Exists
                        row = slot.loc[contact[::-1]]
                        result.append(row[COL_ACCESS])
                        i += 1
                    except KeyError:
                        # Test [A->A] Exists
                        if i > 0 and contact[0] == contact[1]:
                            result.append(result[-1])
                            i += 1
            else:
                break

        # Evaluate te amount of valid contacts
        fitness = len(result)
        duration = np.inf if len(result) < 2 else self.contact_list.iloc[result[-1]][COL_END] - self.contact_list.iloc[result[0]][COL_START]
        individual.set_fitness(fitness, duration, result.copy())

        return fitness

    def fitness_function_lineal(self, individual: Individual):
        """
        Evaluate fitness function

        :param individual: List. Individual to evaluate
        :return: Float.
        """
        sequence = individual.sequence
        contacts = [[sequence[i], sequence[i+1]] for i in range(len(sequence)-1)]
        result = []

        # Evaluate the proposed contact list (find if the proposed contacts exist)
        i = 0
        _row = self.contact_list.iloc[0]
        for _, row in self.contact_list.iterrows():
            if i < len(contacts):
                contact = contacts[i]
                # Contact exist and is valid
                if row[COL_FROM] in contact and row[COL_TO] in contact:
                    result.append(row[COL_ACCESS])
                    i += 1
                # Allow stay in the current node
                elif i > 0 and contact[0] == contact[1]:
                    i += 1
                    result.append(result[-1])
                # The next hop could be the last contact in the opposite direction
                elif _row[COL_FROM] in contact and _row[COL_TO] in contact:
                    result.append(_row[COL_ACCESS])
                    i += 1

                _row = row

            else:
                break

        # Evaluate te amount of valid contacts
        fitness = len(result)
        duration = np.inf if len(result) < 2 else self.contact_list.iloc[result[-1]][COL_END] - self.contact_list.iloc[result[0]][COL_START]
        individual.set_fitness(fitness, duration, result.copy())

        return fitness

    def fitness_function_partial(self, individual: Individual):
        """
        Evaluate fitness function

        :param individual: List. Individual to evaluate
        :return: Float.
        """
        sequence = individual.sequence
        contacts = [[sequence[i], sequence[i+1]] for i in range(len(sequence)-1)]
        result = [0 for _ in contacts]

        for i, contact in enumerate(contacts):
            start = result[i-1] if i > 0 else 0
            for j in range(start, len(self.contact_list)):
                row = self.contact_list.iloc[j]
                # Contact exist and is valid
                if row[COL_FROM] in contact and row[COL_TO] in contact:
                    result[i] = j
                    break
                # Allow stay in the current node
                elif i > 0 and contact[0] == contact[1]:
                    result[i] = result[i-1]
                    break

        # Evaluate te amount of valid contacts
        fitness = np.count_nonzero(result)
        duration = np.inf if len(result) < 2 else self.contact_list.iloc[result[-1]][COL_END] - self.contact_list.iloc[result[0]][COL_START]
        individual.set_fitness(fitness, duration, result.copy())

        return fitness

    def run(self, i=0):
        ti = time.time()
        self.best_individual = GA.run(self)
        self._time = time.time() - ti

        converges = self.best_individual.fitness == len(self.best_individual)-1
        self.contact_plan = self.contact_list.iloc[self.best_individual.contacts]
        if converges:
            self.contact_plan = self.simplify_solution()
        duration = self.contact_plan.iloc[-1][COL_END] - self.contact_plan.iloc[0][COL_START]

        print("{:02}; {}; {}; {}; {}; {}; {}; {}".format(i, self.best_individual.sequence, self.best_individual.contacts, self.len-1, self.best_individual.fitness, duration, self._time, self.current_iteration))
        self.log(self.contact_plan.to_string())

        return self.contact_plan, self.best_individual.fitness, duration, converges

    def simplify_solution(self):
        contacts = self.best_individual.contacts
        sequence = self.best_individual.sequence
        targets_idx = self.best_individual.t_index
        target_nodes = list(np.array(sequence)[targets_idx])
        contact_plan = self.contact_list.iloc[contacts]
        # Fix graph direction (A->B != B->A)
        contacts_fix = np.array([(sequence[i], sequence[i+1]) for i in range(len(sequence)-1)])
        contact_plan = contact_plan.assign(_from=contacts_fix[:, 0], _to=contacts_fix[:, 1])
        # Fix from and to column order
        contact_plan = contact_plan.drop(columns=["from", "to"]).rename(columns={"_from": "from", "_to": "to"})
        # Delete duplicated contacts (including targets) (A->B,B->A, or A->B,A->B)
        contact_plan = contact_plan.drop_duplicates('access')
        # Delete redundant contact (A->B,B->A,A->C => A->C), except for targets
        def drop_redundant(_contact_plan):
            drop_list = []
            for i in range(1, len(_contact_plan)):
                prev_to = _contact_plan.iloc[i-1][COL_TO]
                curr_from = _contact_plan.iloc[i][COL_FROM]
                if prev_to != curr_from and prev_to not in target_nodes:
                    drop_list.append(_contact_plan.iloc[i-1].name)
            return _contact_plan.drop(drop_list)
        contact_plan = drop_redundant(contact_plan)
        return contact_plan

    def plot_results(self, scenario: Scenario = None, figname: str = None, show=True):
        """"
        Show and plot results
        """
        plot_contact_list(self.contact_list, scenario, self.contact_plan, figname=figname, show=show)
        plot_contact_list(self.contact_list, scenario, self.contact_plan, figname=figname+'.2.png', show=show, time=False)

        # Plot fitness values
        fig, axs = plt.subplots(1, sharey=True, sharex=True)
        fig.suptitle("Fitness evolution")
        axs.plot(self._best_fitness_list, label="Max.")
        axs.plot(self._avg_list, label="Avg.")
        axs.grid()
        axs.legend()
        axs.set(xlabel='Generation', ylabel='Fitness')
        if figname:
            plt.savefig(figname+".debug.png")
        if show:
            plt.show()


def get_parameters():
    """
    Parse command line parameters
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("dir", metavar='DIRECTORY', help="Project directory")
    parser.add_argument("scenario", metavar='SCENARIO', help="File with access list")
    parser.add_argument("task", metavar='TASK', help="Task file")
    parser.add_argument("-s", "--size", default=50, type=int, help="Population size")
    parser.add_argument("-m", "--mut", default=0.6, type=float, help="Mutation rate")
    parser.add_argument("-i", "--iter", default=100, type=int, help="Max. iterations")
    parser.add_argument("-n", "--hops", default=5, type=int, help="Max. iterations")
    parser.add_argument("-r", "--runs", default=1, type=int, help="Experiment repetitions")
    parser.add_argument("-p", "--plot", action="store_true", help="Show plots, if not save figure")
    parser.add_argument("-e", "--explore", action="store_true", help="Explore parameters (ignore size and mutation)")
    return parser.parse_args()


if __name__ == "__main__":
    import json
    args = get_parameters()

    working_path = args.dir
    print("Moving to:", working_path)
    prev_cwd = os.getcwd()
    os.chdir(working_path)

    # Load scenario and task definition
    with open(args.scenario) as scenario_file:
        scenario_json = json.load(scenario_file)
    with open(args.task) as task_file:
        task_json = json.load(task_file)

    # Load scenario, task and contact list
    scenario = Scenario(scenario_json)
    task = Task(task_json)
    assert(scenario.contacts is not None)
    contacts = pd.read_csv(scenario.contacts)

    if args.explore:
        sizes = [50, 100, 150, 200]
        mutations = [0.2, 0.4, 0.6, 0.8]
    else:
        sizes = [args.size]
        mutations = [args.mut]

    for size in sizes:
        for mut in mutations:
            case_path = os.path.basename(args.scenario).split('.')[0]
            task_path = os.path.basename(args.task).split('.')[0]
            log_path = "results/{}/{}/m{}_s{}_n{}".format(case_path, task_path, mut, size, args.hops)
            try:
                os.makedirs(log_path)
            except FileExistsError as e:
                print(e)

            # Run Genetic Algorithm
            def run(i=0, plot=args.plot, logfile=log_path+"/log.csv"):
                print("{:02} started. mut={}; size={}, hops={}".format(i, mut, size, args.hops))
                cpd = GeneticCPD(contacts, scenario, task, size, mut, args.iter, False, hops=args.hops)
                contact_plan, fitness, duration, converges = cpd.run(i)
                cpd.plot_results(scenario, figname="{}/{:02}.png".format(log_path, i), show=plot)
                with open(logfile, 'a+') as _logfile:
                    log = "{:02}; {}; {}; {}; {}; {}; {}; {}\n".format(i, cpd.best_individual.sequence, cpd.best_individual.contacts, cpd.len-1, fitness, duration, cpd._time, cpd.current_iteration)
                    _logfile.write(log)


            # list(map(run, list(range(args.runs))))
            with Pool() as pool:
                pool.map(run, list(range(args.runs)))

    print("Finish!")
    os.chdir(prev_cwd)
